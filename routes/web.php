<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('deshboard');
// });

Route::group(['namespace'=>'App\Http\Controllers'], function(){
    #backend
    Route::group(['namespace'=>'Admin','prefix' => 'admin-control','as' => 'admin.'],function(){
        // Route::get('/','LoginController@index')->name('login');
        Route::get('/','DashboardController@dashboard')->name('dashboard');
       

        Route::resources([
            'page' => 'PageController',
            'service'=> 'ServiceController',
            'product'=> 'ProductController',
            'testimonial' =>'TestimonialController',
            'contect' => 'ContectController',
            'contactde' => 'ContactdeController',
            'general' => 'GeneralController',
        ]);
    });

    Route::group(['namespace' => 'Web' ], function(){
        Route::get('/','HomeController@index')->name('home');
        Route::get('about','AboutController@about')->name('about');
        Route::get('product','ProductController@product')->name('product');
        Route::get('contact','ContactController@contact')->name('contact');
        Route::get('{page:slug}', 'PageController@show')->name('page.show');
    });
});