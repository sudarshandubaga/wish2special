 (function () {
            var forms = document.querySelectorAll('.needs-validation');
            var form = forms[0];
            $('.form-control').on('keyup', function (e) {
                // alert();
                form.checkValidity();
                form.classList.add('was-validated');
            });

            $('.needs-validation').on('submit', function (e) {
                // alert();
                if(!form.checkValidity()) {
                    e.preventDefault();
                }
                form.classList.add('was-validated');
            });
        })();