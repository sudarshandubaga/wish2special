$(function () {
	$(document).on("click", ".accordion-box> .drop-submenu-r", function () {
		$(this).closest(".accordion-box").children(".accordion-content").slideToggle();
		$(this).closest(".accordion-box").toggleClass('shown');

		$(this).closest(".accordion-box").siblings().children(".accordion-content").slideUp();
		$(this).closest(".accordion-box").siblings().removeClass('shown');
	});
});