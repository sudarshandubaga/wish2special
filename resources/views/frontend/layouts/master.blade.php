<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#103178" />
    <link rel="shortcut icon" href="assets/images/favicon-32x32.png" type="image/x-icon">
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.css">
    <link rel="stylesheet" href="assets/css/owl.theme.green.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <title>@yield('site_title') | Antivirus Mask-Store</title>
</head>

<body>
    <div class="overly-main"></div>
    <div class="myheadline-top">
        <h3>Due to the COVID 19 epidemic, orders may be processed with a slight delay</h3>
    </div>
    <header>
        <div class="navbar-header-main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <div class="heder-logo">
                            <a href="index.html">
                                <img src="assets/images/logoe.png" alt="logo">
                                <span>CureM4</span>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-5 col-6">
                        <div class="sidebar-btn">
                            <div class="mob-s-y" id="button-enlarge">
                                <div class="bars bar1"></div>
                                <div class="bars bar2"></div>
                                <div class="bars bar3"></div>
                            </div>
                        </div>
                        <div class=" nav-menu-header">
                            <ul>
                                <li><a href="{{route('home')}}">Home</a></li>
                                <li><a href="index.html">Shop</a></li>
                                <li><a href="{{route('product')}}">Products</a></li>
                                <li><a href="{{ route('page.show', ['about-us']) }}">About</a></li>
                                <li><a href="{{route('contact')}}">Contacts</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 mbile-d-n">
                        <div class="socialicon">
                            <h5>Need help? 989-879-0847</h5>
                            <div class="sol-right">
                                <i class='bx bxl-facebook'></i>
                                <i class='bx bxl-twitter'></i>
                                <i class='bx bxl-instagram'></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mobileview mymainulmenumo">
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li><a href="index.html">Shop</a></li>
                    <li><a href="{{route('product')}}">Products</a></li>
                    <li><a href="{{ route('page.show', ['about-us']) }}">About</a></li>
                    <li><a href="contact.html">Contacts</a></li>
                </ul>
            </div>
        </div>
    </header>

@yield('contant')

    <footer class="footer-colorblue">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-6">
                    <div class="mufooterulli">
                        <ul>
                            <li><a href="{{ route('page.show', ['about-us']) }}">About us</a></li>
                            <li><a href="{{ route('page.show', ['delivery']) }}">Delivery</a></li>
                            <li><a href="{{ route('page.show', ['privacy-policy']) }}">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-6">
                    <div class="mufooterulli">
                        <ul>
                            <li><a href="{{ route('page.show', ['affiliate']) }}">Affiliate</a></li>
                            <li><a href="{{ route('page.show', ['sales']) }}">Sales</a></li>
                            <li><a href="{{ route('page.show', ['privacy-policy']) }}">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-6">
                    <div class="mufooterulli">
                        <ul>
                            <li>Bestsellers</li>
                            <li>Discount</li>
                            <li>Latest products</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-6">
                    <div class="mufooterulli">
                        <ul>
                            <li>My account</li>
                            <li>My orders</li>
                            <li>Returns</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="bootomline">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <h4>Copyright 2025 Mobirise. All Rights Reserved</h4>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="imgesocilicon">
                        <img src="assets/images/master-card.png" alt="">
                        <img src="assets/images/union-pay.png" alt="">
                        <img src="assets/images/ebay.png" alt="">
                        <img src="assets/images/american-express.png" alt="">
                        <img src="assets/images/discover.png" alt="">
                        <img src="assets/images/diners-club.png" alt="">
                        <img src="assets/images/bitcoin.png" alt="">
                        <img src="assets/images/amazon.png" alt="">
                        <img src="assets/images/paypal.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script src="assets/js/jquery.min.js "></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.min.js "></script>
    <script src="assets/js/custom.js "></script>



</body>

</html>