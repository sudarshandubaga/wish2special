@extends('frontend.layouts.master')

@section('site_title', $page->title)

@section('contant')
<div class="aboutmain">
        <img src="assets/images/mbr-2.jpg" alt="">
        <div class="overlayimgmyab">
            <h1>{{ $page->title }}</h1>
            <p><a href="index.html">Home</a> / {{ $page->title }}</p>
        </div>
    </div>
    <section class="conta-about">
        <div class="container">
            <p>
                {{ $page->description }}
            </p>
        </div>
    </section>
@endsection