@extends('frontend.layouts.master')

@section('site_title','Home')

@section('contant')
<section class="mask-penal">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="sectionmycontant">
                        <h1>
                            <strong>Antivirus 
                                <br>
                                Mask Store
                        </strong>
                        </h1>
                        <p>The easiest way to protect yourself</p>
                        <div class="virusbe">
                            <ul>
                                <li><i class='bx bx-shape-square'></i>&nbsp;&nbsp;&nbsp;Anti-Bacterial</li>
                                <li><i class='bx bxs-baseball'></i>&nbsp;&nbsp;&nbsp;Anti-Virus</li>
                            </ul>
                        </div>
                        <button>Shop now</button>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="myimagemask">
                        <img src="assets/images/product1.jpg" alt="">
                    </div>
                    <div class="overlaymask">only <span>$25</span></div>
                </div>
            </div>
        </div>
    </section>
    <section class="cardmain-my">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="mycard-img">
                        <img src="assets/images/mbr.jpeg" alt="">
                    </div>
                    <div class="overlayimgcard">
                        <h3>Antivirus <br> Mask</h3>
                        <button>shop now</button>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="mycard-img">
                        <img src="assets/images/card1.jpg" alt="">
                    </div>
                    <div class="overlayimgcard">
                        <h3>Antivirus <br> Mask</h3>
                        <button>shop now</button>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="mycard-img">
                        <img src="assets/images/mbr-3.jpg" alt="">
                    </div>
                    <div class="overlayimgcard">
                        <h3>Antivirus <br> Mask</h3>
                        <button>shop now</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="efective-connect">
        <div class="container">
            <div class="heading-effective">
                <h1>Effective and reliable
                    <br> protection for your</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
            <div class="card-aniver">
                <div class="row">
                    <div class="col-lg-4 p-0 col-md-4">
                        <div class="cardmain">
                            <img src="assets/images/logoe.png" alt="Card image cap">
                            <div class="mycardbd">
                                <h2 class="card-title">Anti-Virus</h2>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 p-0 col-md-4">
                        <div class="cardmain">
                            <img src="assets/images/logoe.png" alt="Card image cap">
                            <div class="mycardbd">
                                <h2 class="card-title">Anti-Virus</h2>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 p-0 col-md-4">
                        <div class="cardmain">
                            <img src="assets/images/logoe.png" alt="Card image cap">
                            <div class="mycardbd">
                                <h2 class="card-title">Anti-Virus</h2>
                                <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="featured-products">
        <h2>Featured products</h2>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="card maycprodectpp">
                        <img class="card-img-top myinmagecard" src="assets/images/mbr-2.jpg" alt="Card image cap">
                        <div class="card-body prodecthdg">
                            <p class="card-text">Medical Mask</p>
                            <h3>$1.99</h3>
                            <i class='bx bx-star'></i>
                            <i class='bx bx-star'></i>
                            <i class='bx bx-star'></i>
                            <i class='bx bx-star'></i>
                            <i class='bx bx-star'></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card maycprodectpp">
                        <img class="card-img-top myinmagecard" src="assets/images/mbr-2.jpg" alt="Card image cap">
                        <div class="card-body prodecthdg">
                            <p class="card-text">Medical Mask</p>
                            <h3>$1.99</h3>
                            <i class='bx bx-star'></i>
                            <i class='bx bx-star'></i>
                            <i class='bx bx-star'></i>
                            <i class='bx bx-star'></i>
                            <i class='bx bx-star'></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card maycprodectpp">
                        <img class="card-img-top myinmagecard" src="assets/images/mbr-2.jpg" alt="Card image cap">
                        <div class="card-body prodecthdg">
                            <p class="card-text">Medical Mask</p>
                            <h3>$1.99</h3>
                            <i class='bx bx-star'></i>
                            <i class='bx bx-star'></i>
                            <i class='bx bx-star'></i>
                            <i class='bx bx-star'></i>
                            <i class='bx bx-star'></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="card maycprodectpp">
                        <img class="card-img-top myinmagecard" src="assets/images/mbr-2.jpg" alt="Card image cap">
                        <div class="card-body prodecthdg">
                            <p class="card-text">Medical Mask</p>
                            <h3>$1.99</h3>
                            <i class='bx bx-star'></i>
                            <i class='bx bx-star'></i>
                            <i class='bx bx-star'></i>
                            <i class='bx bx-star'></i>
                            <i class='bx bx-star'></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="subscribeinputmain">
        <div class="minsubinpu">
            <h3>Join our newsletter and</br>
                get $20 discount</h3>
            <span>
                <input type="text" placeholder="Enter your email address"> <button>subscribe</button>
                </span>
        </div>
    </section>
    <section class="video-mainsection">
        <h3>Watch Video</h3>
    </section>

    @if(!$testimonials->isEmpty())
    <section class="Lates-reviews">
        <h2>Latest reviews</h2>
        <div class="row">

            @foreach($testimonials as $t)
            <div class="col-lg-3 col-md-6">
                <div class="card myreviewcard">
                    <div class="card-body">
                        <p>{{ $t->comment }}</p>
                        <h5 class="card-title">{{ $t->name }}</h5>
                        @for($i = 1; $i <= $t->rating; $i++)
                        <i class='bx bxs-star' style="color: orange"></i>
                        @endfor
                        @for($i = 5; $i > $t->rating; $i--)
                        <i class='bx bx-star'></i>
                        @endfor
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </section>
    @endif

@endsection