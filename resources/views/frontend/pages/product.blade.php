@extends('frontend.layouts.master')

@section ('site_title','Product')

@section('contant')

<div class="aboutmain">
        <img src="assets/images/mbr-2.jpg" alt="">
        <div class="overlayimgmyab">
            <h1>Products</h1>
            <p><a href="index.html">Home</a> / Products</p>
        </div>
    </div>
    <section class="Products-pa">
        <section class="featured-products">
            <h2>Featured products</h2>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <div class="card my-cardpro">
                            <img class="card-img-top myinmagecard" src="assets/images/mbr-2.jpg" alt="Card image cap">
                            <div class="card-body mycoustumbis prodecthdg">
                                <p class="card-text">Medical Mask</p>
                                <h3>$1.99</h3>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="card my-cardpro">
                            <img class="card-img-top myinmagecard" src="assets/images/mbr-2.jpg" alt="Card image cap">
                            <div class="card-body mycoustumbis prodecthdg">
                                <p class="card-text">Medical Mask</p>
                                <h3>$1.99</h3>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="card my-cardpro">
                            <img class="card-img-top myinmagecard" src="assets/images/mbr-2.jpg" alt="Card image cap">
                            <div class="card-body mycoustumbis prodecthdg">
                                <p class="card-text">Medical Mask</p>
                                <h3>$1.99</h3>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="card my-cardpro">
                            <img class="card-img-top myinmagecard" src="assets/images/mbr-2.jpg" alt="Card image cap">
                            <div class="card-body mycoustumbis prodecthdg">
                                <p class="card-text">Medical Mask</p>
                                <h3>$1.99</h3>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="card my-cardpro">
                            <img class="card-img-top myinmagecard" src="assets/images/mbr-2.jpg" alt="Card image cap">
                            <div class="card-body mycoustumbis prodecthdg">
                                <p class="card-text">Medical Mask</p>
                                <h3>$1.99</h3>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="card my-cardpro">
                            <img class="card-img-top myinmagecard" src="assets/images/mbr-2.jpg" alt="Card image cap">
                            <div class="card-body mycoustumbis prodecthdg">
                                <p class="card-text">Medical Mask</p>
                                <h3>$1.99</h3>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="card my-cardpro">
                            <img class="card-img-top myinmagecard" src="assets/images/mbr-2.jpg" alt="Card image cap">
                            <div class="card-body mycoustumbis prodecthdg">
                                <p class="card-text">Medical Mask</p>
                                <h3>$1.99</h3>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <div class="card my-cardpro">
                            <img class="card-img-top myinmagecard" src="assets/images/mbr-2.jpg" alt="Card image cap">
                            <div class="card-body mycoustumbis prodecthdg">
                                <p class="card-text">Medical Mask</p>
                                <h3>$1.99</h3>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                                <i class='bx bx-star'></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </section>
@endsection