@extends('frontend.layouts.master')

@section('site_title','contact')

@section('contant')

<div class="aboutmain">
    <img src="assets/images/mbr-2.jpg" alt="">
    <div class="overlayimgmyab">
        <h1>Contact Us</h1>
        <p><a href="index.html">Home</a> / Contact Us</p>
    </div>
</div>
<section class="contectuspage">
    <div class="container">
        
         <!-- message -->
    @if (session('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ session('success') }}
        <strong>Success </strong> Thanks for being awesome! 
        We have received your message and would like to thank you for writing to  your inquiry 
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <!-- message -->

        <div class="row">
            <div class="col-lg-6">
                <div class="form-contect-detai">
                    <div>
                        <i class='bx bx-phone'></i>
                        <br>
                        <br>
                        <i class='bx bx-envelope'></i>
                        <br>
                        <br>
                        <i class='bx bx-location-plus'></i>
                    </div>
                    <div class="mycontev">
                        <span>+91-8989897162</span>
                        <br>
                        <br>
                        <span>abc@gmaile.com</span>
                        <br>
                        <br>
                        <span>P.No. 324-25, 378-79-80, khasra No. 9/4, Shree Yade Gaun, Near Banar Ring Road, Jodhpur (Rajasthan) - 342027</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="my-contform">
                    <form action="{{ route('admin.contect.store') }}" method="post">
                        @csrf
                        <div class="contact-innaer">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" placeholder="Name*">
                            </div>
                            <div class="form-group">
                                <input type="text" name="email" class="form-control" placeholder="Email*">
                            </div>
                            <div class="form-group">
                                <input type="text" name="number" class="form-control" placeholder="Phone Number*">
                            </div>
                            <div class="form-group">
                                <textarea name="message" class="form-control" rows="6" placeholder="Please describe what you need."></textarea>
                            </div>
                            <button type="submit" class="btn btn-outline-primary mybuttonc btn-lg">Send message</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection