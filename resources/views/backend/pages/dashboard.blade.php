@extends('backend.layouts.inner')

@section('deshborad_title','deshborad')

@section('contant')
      
            <main class="main-i">
               <div class="container-fluid">
                  <h1 class="mt-4 titl-fo">Dashboard</h1>
                  <ol class="breadcrumb mb-4">
                     <li class="breadcrumb-item  active">Dashboard</li>
                  </ol>
                  <div class="row">
                     <div class="col-xl-6">
                        <div class="card mb-4">
                           <div class="card-header"> <i class="fas fa-chart-area mr-1"></i> Area Chart Example</div>
                           <div class="card-body">
                              <canvas id="myChart" class="canvas-chart-i"></canvas>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-6">
                        <div class="card mb-4">
                           <div class="card-header"> <i class="fas fa-chart-bar mr-1"></i> Bar Chart Example</div>
                           <div class="card-body">
                              <canvas id="myChart2" class="canvas-chart-i"></canvas>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </main>
            @endsection
    