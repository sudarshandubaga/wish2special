@extends('backend.layouts.inner')

@section('deshborad_title','view')

@section('contant')

            <main class="main-i">
               <div class="container-fluid">
                  <div class="first-title-topadd">
                     <div class="">
                        <div class="row">
                           <div class="col-lg-6">
                              <div class="title-left-top-table">
                                 <h4>Promotion / Increament</h4>
                                 <small><a href="#">Deshborad</a><i class='bx bx-caret-right'></i><a href="#">Payroll Management</a><i class='bx bx-caret-right'></i><span>Promotion / Increament</span></small>
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="buton-add-new">
                              <a href="{{url('admin-control/addpage')}}">
                                 <button type="button"><i class='bx bx-plus'>Add</i></button>
                              </a>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <section class="recent">
                     <div class="activiti-card">
                        <div class="search-top-table">
                           <div class="row">
                              <div class="col-lg-6">
                                 <div class="searchbarleft">
                                    <input type="text" class="search-input-table" name="Search" placeholder="Search-Filter">
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="table-head-right">
                                    <button type="button"><i class='bx bxs-trash'></i></button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="table-responsiveima">
                           <table>
                              <thead class="the-colo-table">
                                 <tr>
                                    <th><input type="checkbox"  name="sr.no" >Sr.No</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Team</th>
                                    <th>Status</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td><input type="checkbox"  name="sr.no" >1</td>
                                    <td>15 Aug, 2020</td>
                                    <td>22 aug, 2020</td>
                                    <td>22 aug, 2020</td>
                                    <td> <span class="badge success">Success</span>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox"  name="sr.no" >2</td>
                                    <td>15 Aug, 2020</td>
                                    <td>22 aug, 2020</td>
                                    <td>22 aug, 2020</td>
                                    <td> <span class="badge warning">Success</span>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox"  name="sr.no" >3</td>
                                    <td>15 Aug, 2020</td>
                                    <td>22 aug, 2020</td>
                                    <td>22 aug, 2020</td>
                                    <td> <span class="badge success">Success</span>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox"  name="sr.no" >4</td>
                                    <td>15 Aug, 2020</td>
                                    <td>22 aug, 2020</td>
                                    <td>22 aug, 2020</td>
                                    <td> <span class="badge warning">Success</span>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox"  name="sr.no" >5</td>
                                    <td>15 Aug, 2020</td>
                                    <td>22 aug, 2020</td>
                                    <td>22 aug, 2020</td>
                                    <td> <span class="badge success">Success</span>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td><input type="checkbox"  name="sr.no" >6</td>
                                    <td>15 Aug, 2020</td>
                                    <td>22 aug, 2020</td>
                                    <td>22 aug, 2020</td>
                                    <td> <span class="badge success">Success</span>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </section>
               </div>
            </main>
      @endsection