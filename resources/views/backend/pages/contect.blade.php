@extends('backend.layouts.inner')

@section('deshborad_title','view')

@section('contant')

    <main class="main-i">
        <div class="container-fluid">
            <div class="first-title-topadd">
                <div class="">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="title-left-top-table">
                            <h4>View Contact</h4>
                            <small><a href="#">Deshborad</a><i class='bx bx-caret-right'></i><span>Contact</span></small>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="buton-add-new">
                        <a href="{{url('admin-control/contect')}}">
                            <button type="button">View</button>
                        </a>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <section class="recent">
                <div class="activiti-card">
                <div class="search-top-table">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="searchbarleft">
                            <input type="text" class="search-input-table" name="Search" placeholder="Search-Filter">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="table-head-right">
                            <button type="button"><i class='bx bxs-trash'></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsiveima">
                    <table>
                        <thead class="the-colo-table">
                            <tr>
                            <th><input type="checkbox"  name="sr.no" >Sr.No</th>
                            <th>Name</th>
                            <th>Phone Number</th>
                            <th>Email Address</th>
                            <th>Message</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($contects as $index => $contect)
                            <tr>
                                <td><input type="checkbox"  name="sr.no" >{{ $index + $contects->firstItem() }}.</td>
                                <td>{{ $contect->name }}</td>
                                <td>{{ $contect->number }}</td>
                                <td>{{ $contect->email}}</td>
                                <td>{{ $contect->message}}</td>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $contects->links("pagination::bootstrap-4") }}
                </div>
            </section>
        </div>
    </main>
@endsection