@extends('backend.layouts.inner')

@section('deshborad_title','view')

@section('contant')

    <main class="main-i">
        <div class="container-fluid">
            <div class="first-title-topadd">
                <div class="">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="title-left-top-table">
                            <h4>Pages / Page</h4>
                            <small><a href="#">Deshborad</a><i class='bx bx-caret-right'></i><span>Page</span></small>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="buton-add-new">
                        <a href="{{route('admin.page.create')}}">
                            <button type="button"><i class='bx bx-plus'>Add</i></button>
                        </a>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <section class="recent">
                <div class="activiti-card">
                <div class="search-top-table">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="searchbarleft">
                            <input type="text" class="search-input-table" name="Search" placeholder="Search-Filter">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="table-head-right">
                            <button type="button"><i class='bx bxs-trash'></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsiveima">
                    <table>
                        <thead class="the-colo-table">
                            <tr>
                            <th><input type="checkbox"  name="sr.no" >Sr.No</th>
                            <th>Title</th>
                            <th>Short Description</th>
                            <th>Image</th>
                            <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pages as $index => $page)
                            <tr>
                                <td><input type="checkbox"  name="sr.no" >{{ $index + $pages->firstItem() }}.</td>
                                <td>{{ $page->title }}</td>
                                <td>{{ $page->excerpt }}</td>
                                <td>
                                <img src="{{ url('storage/' . $page->image) }}" alt="" style="width: 100px; height: 100px; object-fit: contain;">
                                </td>
                               
                            </td>
                            <td>
                                 <div class="dropdown">
                                    <div id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class='bx bx-dots-vertical-rounded'></i>
                                    </div>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item " href="{{route('admin.page.edit',[$page->id])}}">Edit</a></li>
                                       <li> {{ Form::open(['url' => route('admin.page.destroy', [$page->id]), 'method' => 'delete']) }}
                                            <button class="dropdown-item">Delete</button>
                                            {{ Form::close() }}
                                        </li>
                                    
                                    </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $pages->links("pagination::bootstrap-4") }}
                </div>
            </section>
        </div>
    </main>
@endsection