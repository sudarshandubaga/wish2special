<div class="row">
    <div class="col-lg-8">
        <div class="main-form-deco">
            <div class="row">
                <div class="col-lg-6">
                    <div class="formleftadd mbinput">
                        {!! Form::label('title')!!}
                        {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Enter Title']) !!}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="formleftadd mbinput">
                        {!! Form::label('slug')!!}
                        {!! Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Enter Slug']) !!}
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="formleftadd mbinput">
                        {!! Form::label('excerpt')!!}
                        {!! Form::textarea('excerpt', null, ['class' => 'form-control','placeholder' => 'Enter Short Description', 'rows' => '2'])!!}
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="formleftadd mbinput">
                        {!! Form::label('description')!!}
                        {!! Form::textarea('description', null, ['class' => 'form-control','placeholder' => 'Enter Description', 'rows' => '4'])!!}
                    </div>
                </div>

            </div>

        </div>
    </div>

    <div class="col-lg-4">
        @if(!empty($page->image))
        <div class="card " style="width: 18rem;">
            <img src="{{url('storage/' . $page->image)}}" class="card-img-top" alt="..." name="image">
        </div>
        @endif

        <label class="btn btn-block btn-primary mt-3">
            {{ Form::file('image', ['style' => 'display: none;']) }}
            Choose image
        </label>
    </div>
    <div class="mybutton-sub-re">
        <button type="submit" class="btn btn-primary">{{ empty($page) ? 'save' : 'update' }}</button>
        <input class="btn btn-danger" type="reset" value="Reset">
    </div>
    <div>
        <div></div>
    </div>
</div>