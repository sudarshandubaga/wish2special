@extends('backend.layouts.inner')

@section('deshborad_title','view')

@section('contant')

    <main class="main-i">
        <div class="container-fluid">
            <div class="first-title-topadd">
                <div class="">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="title-left-top-table">
                            <h4>View Services</h4>
                            <small><a href="#">Deshborad</a><i class='bx bx-caret-right'></i><span>Service</span></small>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="buton-add-new">
                        <a href="{{route('admin.service.create')}}">
                            <button type="button"><i class='bx bx-plus'>Add</i></button>
                        </a>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <section class="recent">
                <div class="activiti-card">
                <div class="search-top-table">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="searchbarleft">
                            <input type="text" class="search-input-table" name="Search" placeholder="Search-Filter">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="table-head-right">
                            <button type="button"><i class='bx bxs-trash'></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsiveima">
                    <table>
                        <thead class="the-colo-table">
                            <tr>
                            <th><input type="checkbox"  name="sr.no" >Sr.No</th>
                            <th>Title</th>
                            <th>Short Description</th>
                            <th>Description</th>
                            <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($services as $index => $service)
                            <tr>
                                <td><input type="checkbox"  name="sr.no" >{{ $index + $services->firstItem() }}.</td>
                                <td>{{ $service->title }}</td>
                                <td>{{ $service->excerpt }}</td>
                                <td>{{ $service->description}}</td>
                                <td> <span class="badge success">Success</span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $services->links("pagination::bootstrap-4") }}
                </div>
            </section>
        </div>
    </main>
@endsection