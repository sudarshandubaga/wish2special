@extends('backend.layouts.inner')

@section('deshborad_title','Add Page')

@section('contant')

<main class="main-i">
   <div class="container-fluid">
      <div class="first-title-topadd2">

         <div class="row">
            <div class="col-lg-6">
               <div class="title-left-top-table">
                  <h4>Add Services</h4>
                  <small><a href="#">Deshborad</a><i class="bx bx-caret-right"></i><span>Service</span></small>
               </div>
            </div>
            <div class="col-lg-6">
               <div class="buton-add-new">
                  <a href="{{route('admin.service.index')}}">
                     <button type="button">View</button>
                  </a>
               </div>
            </div>
         </div>
      </div>
      <div class="main-addpage-main">
         <form action="{{ route('admin.service.store') }}" method="post">
            @csrf
            <div class="row">
               <div class="col-lg-8">
                  <div class="main-form-deco">
                     <div class="mbinput">
                        <div class="row">
                           <div class="col-lg-6">
                              <div class="formleftadd">
                                 <label><small>Title</small></label><br />
                                 <input type="text" name="title" placeholder="Title" class="form-contrinpuol">
                              </div>
                           </div>
                           <div class="col-lg-6">
                              <div class="formleftadd">
                                 <label><small>Slug</small></label><br />
                                 <input type="text" name="slug" placeholder="Slug" class="form-contrinpuol">
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="mbinput">
                        <div class="row">
                           <div class="col-lg-12">
                              <div class="formleftadd">
                                 <label><small>short Description</small></label><br />
                                 <textarea name="short_description" class="form-selectcontrinpuol" placeholder="Short Description" rows="4"></textarea>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div class="mbinput">
                        <div class="row">
                           <div class="col-lg-12">
                              <div class="formleftadd">
                                 <label><small>Description</small></label><br />
                                 <textarea name="description" class="form-selectcontrinpuol" placeholder="Description" rows="6"></textarea>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div class="right-form-img">
                     <label for="file">
                        <img src="{{url('admin/assets/img/download.png')}}" class="img-fluid">
                     </label>
                     <input type="file" id="file" name="file">
                  </div>
               </div>
               <div class="mybutton-sub-re">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <input class="btn btn-danger" type="reset" value="Reset">
               </div>
            </div>
         </form>
      </div>
   </div>
</main>
@endsection