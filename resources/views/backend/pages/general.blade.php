@extends('backend.layouts.inner')

@section('deshborad_title','Add Page')

@section('contant')

@if (session('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ session('success') }}
    <strong>You have successfully</strong>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>

@endif


<main class="main-i">
    <div class="container-fluid">
        <div class="first-title-topadd2">

            <div class="row">
                <div class="col-lg-6">
                    <div class="title-left-top-table">
                        <h4>General setting</h4>
                        <small><a href="#">Deshborad</a><i class="bx bx-caret-right"></i><span>General setting</span></small>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="buton-add-new">
                        <a href="{{route('admin.service.index')}}">
                            <button type="button">View</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-addpage-main">
            <form action="{{ route('admin.general.store') }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-lg-8">
                        <div class="main-form-deco">
                            <div class="mbinput">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="formleftadd mbinput">
                                            <label><small>Site Title</small></label><br />
                                            <input type="text" name="site_title" placeholder="Site title" class="form-contrinpuol">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="formleftadd mbinput">
                                            <label><small>Site Tagline</small></label><br />
                                            <input type="text" name="site_tagline" placeholder="Site Tagline" class="form-contrinpuol">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="formleftadd mbinput">
                                            <label><small>Header Text</small></label><br />
                                            <textarea name="header_des" class="form-selectcontrinpuol" placeholder="Address" rows="4"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="formleftadd mbinput">
                                            <label><small>Upload permotion videos</small></label><br />
                                            <input type="text" name="video_link" placeholder="Upload videos link" class="form-contrinpuol">
                                        </div>
                                    </div>
                                  
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="right-form-img">
                        <h5>Logo</h5>
                            <label for="logo">
                                <img src="{{url('admin/assets/img/download.png')}}" class="img-fluid">
                            </label>
                            <input type="file" id="logo" name="logo">
                        </div>
                        <div class="right-form-img">
                        <h6>Favicon</h6>
                            <label for="favicon">
                                <img src="{{url('admin/assets/img/download.png')}}" class="img-fluid">
                            </label>
                            <input type="file" id="favicon" name="favicon">
                        </div>
                    </div>
                    <div class="mybutton-sub-re">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <input class="btn btn-danger" type="reset" value="Reset">
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>
@endsection