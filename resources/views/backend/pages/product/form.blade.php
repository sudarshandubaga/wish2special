<div class="row">
    <div class="col-lg-8">
        <div class="main-form-deco">
            <div class="row">
                <div class="col-lg-6">
                    <div class="formleftadd mbinput">
                        {!! Form::label('title') !!}
                        {!! Form::text('title', null, ['class' => 'form-control','placeholder' => 'Enter Title']) !!}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="formleftadd mbinput">
                        {!! Form::label('Slug') !!}
                        {!! Form::text('slug', null, ['class' => 'form-control','placeholder' => 'Enter slug']) !!}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="formleftadd mbinput">
                        {!! Form::label('Price') !!}
                        {!! Form::number('price', null, ['class' => 'form-control','placeholder' => 'Enter price']) !!}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="formleftadd mbinput">
                        <label><small>Start Rating</small></label><br />
                       {!! Form::select('rating', [ '1' => '1star', '2' => '2star', '3' => '3star','4' =>'4star','5' => '5star'],null,['class' => 'form-control','placeholder' => 'select price']) !!}
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="formleftadd">
                        {!! Form::label('Description') !!}
                        {!! Form::textarea('description', null, ['class' => 'form-control','placeholder' => 'Enter price']) !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-lg-4">
        <div class="right-form-img">
            <label for="file">
                <img src="{{url('admin/assets/img/download.png')}}" class="img-fluid">
            </label>
            <input type="file" id="file" name="file">
        </div>
    </div>
    <div class="mybutton-sub-re">
        <button type="submit" class="btn btn-primary">{{ empty($product) ? 'save' : 'update' }}</button>
        <input class="btn btn-danger" type="reset" value="Reset">
    </div>
</div>