@extends('backend.layouts.inner')

@section('deshborad_title','Edit Page')

@section('contant')

<main class="main-i">
   <div class="container-fluid">
      <div class="first-title-topadd2">

         <div class="row">
            <div class="col-lg-6">
               <div class="title-left-top-table">
                  <h4>Edit Product</h4>
                  <small><a href="#">Deshborad</a><i class="bx bx-caret-right"></i><span>Product {{ $product->title }}</span></small>
               </div>
            </div>
            <div class="col-lg-6">
               <div class="buton-add-new">
                  <a href="{{route('admin.product.index')}}">
                     <button type="button">View</button>
                  </a>
               </div>
            </div>
         </div>
      </div>
      <div class="main-addpage-main">
        
         {!! Form::open(['url' => route('admin.product.update',[$product->id]),'method' =>'put']) !!}
            @include('backend.pages.product.form')
          {!! Form::close() !!} 
      </div>    
</main>
@endsection