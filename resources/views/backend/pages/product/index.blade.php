@extends('backend.layouts.inner')

@section('deshborad_title','view')

@section('contant')

    <main class="main-i">
        <div class="container-fluid">
            <div class="first-title-topadd">
                <div class="">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="title-left-top-table">
                            <h4>View Product</h4>
                            <small><a href="#">Deshborad</a><i class='bx bx-caret-right'></i><span>Page</span></small>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="buton-add-new">
                        <a href="{{route('admin.product.create')}}">
                            <button type="button"><i class='bx bx-plus'>Add</i></button>
                        </a>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <section class="recent">
                <div class="activiti-card">
                <div class="search-top-table">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="searchbarleft">
                            <input type="text" class="search-input-table" name="Search" placeholder="Search-Filter">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="table-head-right">
                            <button type="button"><i class='bx bxs-trash'></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsiveima">
                    <table>
                        <thead class="the-colo-table">
                            <tr>
                            <th><input type="checkbox"  name="sr.no" >Sr.No</th>
                            <th>Title</th>
                            <th>Price</th>
                            <th>Rating</th>
                            <th>Image</th>
                            <th>Description</th>
                            <th>status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $index => $product)
                            <tr>
                                <td><input type="checkbox"  name="sr.no" >{{ $index + $products->firstItem() }}.</td>
                                <td>{{ $product->title }}</td>
                                <td>{{ $product->price }}</td>
                                <td>
                                    @for($i = 1; $i <= $product->rating; $i++)
                                    <i class='bx bxs-star' style="color: orange"></i>
                                    @endfor
                                    @for($i = 5; $i > $product->rating; $i--)
                                    <i class='bx bx-star'></i>
                                    @endfor
                                </td>
                                <td>{{ $product->images}}</td>
                                <td>{{ $product->description}}</td>
                                <td>
                                 <div class="dropdown">
                                    <div id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class='bx bx-dots-vertical-rounded'></i>
                                    </div>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item " href="{{route('admin.product.edit',[$product->id])}}">Edit</a></li>
                                        <li><a class="dropdown-item" href="#">Delete</a></li>
                                    </ul>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $products->links("pagination::bootstrap-4") }}
                </div>
            </section>
        </div>
    </main>
@endsection