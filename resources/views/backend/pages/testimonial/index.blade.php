@extends('backend.layouts.inner')

@section('deshborad_title','view')

@section('contant')

    <main class="main-i">
        <div class="container-fluid">
            <div class="first-title-topadd">
                <div class="">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="title-left-top-table">
                            <h4>View Testimonial</h4>
                            <small><a href="#">Deshborad</a><i class='bx bx-caret-right'></i><span>Testimonial</span></small>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="buton-add-new">
                        <a href="{{route('admin.testimonial.create')}}">
                            <button type="button"><i class='bx bx-plus'>Add</i></button>
                        </a>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <section class="recent">
                <div class="activiti-card">
                <div class="search-top-table">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="searchbarleft">
                            <input type="text" class="search-input-table" name="Search" placeholder="Search-Filter">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="table-head-right">
                            <button type="button"><i class='bx bxs-trash'></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsiveima">
                    <table>
                        <thead class="the-colo-table">
                            <tr>
                            <th><input type="checkbox"  name="sr.no" >Sr.No</th>
                            <th>Name</th>
                            <th>Rating</th>
                            <th>Image</th>
                            <th>comments</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                            @foreach($testimonials as $index => $testimonial)
                            <tr>
                                <td><input type="checkbox"  name="sr.no" >{{ $index + $testimonials->firstItem() }}.</td>
                                <td>{{ $testimonial->name }}</td>
                                <td>
                                    @for($i = 1; $i <= $testimonial->rating; $i++)
                                    <i class='bx bxs-star' style="color: orange"></i>
                                    @endfor
                                    @for($i = 5; $i > $testimonial->rating; $i--)
                                    <i class='bx bx-star'></i>
                                    @endfor
                                </td>
                                <td>{{ $testimonial->images}}</td>
                                <td>{{ $testimonial->comment}}</td>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $testimonials->links("pagination::bootstrap-4") }}
                </div>
            </section>
        </div>
    </main>
@endsection