@extends('backend.layouts.inner')

@section('deshborad_title','Add Page')

@section('contant')

<main class="main-i">
   <div class="container-fluid">
      <div class="first-title-topadd2">

         <div class="row">
            <div class="col-lg-6">
               <div class="title-left-top-table">
                  <h4>Add Testimonial</h4>
                  <small><a href="#">Deshborad</a><i class="bx bx-caret-right"></i><span>Testimonial</span></small>
               </div>
            </div>
            <div class="col-lg-6">
               <div class="buton-add-new">
                  <a href="{{route('admin.testimonial.index')}}">
                     <button type="button">View</button>
                  </a>
               </div>
            </div>
         </div>
      </div>
      <div class="main-addpage-main">
         <form action="{{ route('admin.testimonial.store') }}" method="post">
            @csrf
            <div class="row">
               <div class="col-lg-8">
                  <div class="main-form-deco">
                     <div class="row">
                        <div class="col-lg-6">
                           <div class="formleftadd mbinput">
                              <label><small>Name</small></label><br />
                              <input type="text" name="name" placeholder="Title" class="form-contrinpuol">
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="formleftadd mbinput">
                              <label><small>Slug</small></label><br />
                              <input type="text" name="slug" placeholder="Slug" class="form-contrinpuol">
                           </div>
                        </div>
                        <div class="col-lg-12">
                           <div class="formleftadd mbinput">
                              <label><small>Start Rating</small></label><br />
                              <select name="rating" class="form-selectcontrinpuol ">
                                 <option value="1">1.   star</option>
                                 <option value="2">2.   star</option>
                                 <option value="3">3.   star</option>
                                 <option value="4">4.   star</option>
                                 <option value="5">5.   star</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-lg-12">
                           <div class="formleftadd">
                              <label><small>Comments</small></label><br />
                              <textarea name="comments" class="form-selectcontrinpuol" placeholder="Comments" rows="6"></textarea>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
               <div class="col-lg-4">
                  <div class="right-form-img">
                     <label for="file">
                        <img src="{{url('admin/assets/img/download.png')}}" class="img-fluid">
                     </label>
                     <input type="file" id="file" name="file">
                  </div>
               </div>
               <div class="mybutton-sub-re">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <input class="btn btn-danger" type="reset" value="Reset">
               </div>
            </div>
         </form>
      </div>
   </div>
</main>
@endsection