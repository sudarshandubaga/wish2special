@extends('backend.layouts.inner')

@section('deshborad_title','Add Page')

@section('contant')

@if (session('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
{{ session('success') }}
  <strong>You have successfully</strong>
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>

@endif


<main class="main-i">
    <div class="container-fluid">
        <div class="first-title-topadd2">

            <div class="row">
                <div class="col-lg-6">
                    <div class="title-left-top-table">
                        <h4>Contact Details</h4>
                        <small><a href="#">Deshborad</a><i class="bx bx-caret-right"></i><span>Contact Details</span></small>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="buton-add-new">
                        <a href="{{route('admin.service.index')}}">
                            <button type="button">View</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-addpage-main">
            <form action="{{ route('admin.contactde.store') }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-lg-8">
                        <div class="main-form-deco">
                            <div class="mbinput">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="formleftadd mbinput">
                                            <label><small>Mobile Number</small></label><br />
                                            <input type="text" name="number" placeholder="Mobile number" class="form-contrinpuol">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="formleftadd mbinput">
                                            <label><small>Email</small></label><br />
                                            <input type="text" name="email" placeholder="Email Address" class="form-contrinpuol">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="formleftadd mbinput">
                                            <label><small>Address</small></label><br />
                                            <textarea name="address" class="form-selectcontrinpuol" placeholder="Address" rows="4"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="formleftadd mbinput">
                                            <label><small>Facebook Link</small></label><br />
                                            <input type="text" name="facebooklink" placeholder="Facebook Link" class="form-contrinpuol">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="formleftadd mbinput">
                                            <label><small>Instagram Link</small></label><br />
                                            <input type="text" name="instagramlink" placeholder="instagram Link" class="form-contrinpuol">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="formleftadd ">
                                            <label><small>Twitter Link</small></label><br />
                                            <input type="text" name="twitterlink" placeholder="Twitter Link" class="form-contrinpuol">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="right-form-img">
                            <label for="file">
                                <img src="{{url('admin/assets/img/download.png')}}" class="img-fluid">
                            </label>
                            <input type="file" id="file" name="file">
                        </div>
                    </div>
                    <div class="mybutton-sub-re">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <input class="btn btn-danger" type="reset" value="Reset">
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>
@endsection