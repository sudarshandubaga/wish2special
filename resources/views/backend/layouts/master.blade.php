<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
      <meta name="theme-color" content="#000000"/>  
      <title>Dashboard | Admin Panel</title>
      <link rel="stylesheet" type="text/css" href="admin/assets/fonts/font-awesome.min.css">
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
      <link rel="stylesheet" type="text/css" href="{{url('admin/assets/css/bootstrap.min.css')}}">
      <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
      <link rel="stylesheet" href="{{url('admin/assets/css/style.css')}}">
        <link rel="stylesheet" href="{{url('admin/assets/css/responsive.css')}}">
    </head>
    <body class="body-login-pagr">

    @yield('contant')
        
    
       
      <script src="assets/js/bootstrap.bundle.min.js" defer></script>
      <script src="assets/js/jquery.min.js" defer></script>
      <script src="assets/js/script.js" defer></script>
    </body>
</html>
