<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
   <meta name="theme-color" content="#000000" />
   <title>@yield('deshborad_title') | Admin Panel</title>
   <link rel="stylesheet" type="text/css" href="{{url('admin/assets/fonts/font-awesome.min.css')}}">
   <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/lykmapipo/themify-icons@0.1.2/css/themify-icons.css">
   <link rel="stylesheet" type="text/css" href="{{url('admin/assets/css/bootstrap.min.css')}}">
   <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
   <link rel="stylesheet" href="{{url('admin/assets/css/style.css')}}">
   <link rel="stylesheet" href="{{url('admin/assets/css/responsive.css')}}">
</head>

<body>
   <div class="main-des-u">
      <div class="main-contan-i">
         <div class="barheder">
            <header class="header " id="header">
               <div class="heder-dark">
                  <div class="container-fluid">
                     <div class="row">
                        <div class="col-lg-2 col-5">
                           <div class="title-logo">Mask<span> Store</span>
                           </div>
                        </div>
                        <div class="col-lg-2 col-2">
                           <div class="sidebar-btn">
                              <div class=" mob-s-y change" id="button-enlarge"></i>
                                 <div class="bars"></div>
                                 <div class="bars"></div>
                                 <div class="bars"></div>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-8 col-5">
                           <div class="navbar-icon-heder">
                              <ul>
                                 <li class="nav-item dropdown hover-colo ">

                                    <a class="dropdown-toggle nav-my-co " href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"> <i class='bx bxs-user' data-bs-toggle="tooltip" data-bs-placement="bottom" title="Profile"></i>
                                    </a>

                                    <ul class="dropdown-menu drop-hover-i" aria-labelledby="navbarDropdown">
                                       <li><a class="dropdown-item" href="{{url('admin-control/viewpage')}}">Settings</a>
                                       </li>
                                       <li><a class="dropdown-item" href="#">Activity Log</a>
                                       </li>
                                       <li>
                                          <hr class="dropdown-divider">
                                       </li>
                                       <li><a class="dropdown-item" href="{{url('admin-control')}}">Logout</a>
                                       </li>
                                    </ul>
                                 </li>
                              </ul>
                           </div>
                           <div class="navbar-icon-heder2">
                              <ul>
                                 <li class="nav-item dropdown hover-colo">
                                    <a class=" nav-my-co" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false"> <i class='bx bxs-bell' data-bs-toggle="tooltip" data-bs-placement="bottom" title="notification"></i>
                                    </a>
                                    <ul class="dropdown-menu  drop-hover-i" aria-labelledby="navbarDropdown">
                                       <li><a class="dropdown-item" href="#">Action</a>
                                       </li>
                                       <li><a class="dropdown-item" href="#">Another action</a>
                                       </li>
                                       <li>
                                          <hr class="dropdown-divider">
                                       </li>
                                       <li><a class="dropdown-item" href="#">Something else here</a>
                                       </li>
                                    </ul>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </header>
         </div>
         <div class="overly-main"></div>
         <div class="sidebar mobileview">
            <!-- sidebar -->
            <div class="l-navbar sho" id="nav-bar">
               <nav class="navor mynave-listt">
                  <div class="accordion">
                     <ul>
                        <li>
                           <div class="main-deshbord-heding"> <span class="bx bxs-dashboard nav-icon" data-bs-toggle="tooltip" data-bs-placement="right" title="Dashboard"></span>
                              <a href="{{('deshborad')}}" class="nav-text">Dashboard</a>
                           </div>
                        </li>

                        <li class="has-dropdown accordion-box">
                           <div class="drop-submenu-r"> <span class="bx bx-grid-alt nav-icon" data-bs-toggle="tooltip" data-bs-placement="right" title="Dashboard">
                                 <!--   <div class="hover-nav-bar-icon">rdfgf</div> -->
                              </span>
                              <span class="nav-text">Pages</span>
                              <a href=""></a>
                           </div>
                           <div class="accordion-content">
                              <ul>
                                 <li> <a href="{{route('admin.page.create')}}">Add</a></li>
                                 <li> <a href="{{route('admin.page.index')}}">View</a></li>
                              </ul>
                           </div>
                        </li>
                        <li class="has-dropdown accordion-box">
                           <div class="drop-submenu-r"> <span class="bx bx-grid-alt nav-icon" data-bs-toggle="tooltip" data-bs-placement="right" title="Dashboard">
                                 <!--   <div class="hover-nav-bar-icon">rdfgf</div> -->
                              </span>
                              <span class="nav-text">Services</span>
                              <a href=""></a>
                           </div>
                           <div class="accordion-content">
                              <ul>
                                 <li> <a href="{{route('admin.service.create')}}">Add</a></li>
                                 <li> <a href="{{route('admin.service.index')}}">View</a></li>
                              </ul>
                           </div>
                        </li>
                        <li class="has-dropdown accordion-box">
                           <div class="drop-submenu-r"> <span class="bx bx-grid-alt nav-icon" data-bs-toggle="tooltip" data-bs-placement="right" title="Dashboard">
                                 <!--   <div class="hover-nav-bar-icon">rdfgf</div> -->
                              </span>
                              <span class="nav-text">Products</span>
                              <a href=""></a>
                           </div>
                           <div class="accordion-content">
                              <ul>
                                 <li> <a href="{{route('admin.product.create')}}">Add</a></li>
                                 <li> <a href="{{route('admin.product.index')}}">View</a></li>
                              </ul>
                           </div>
                        </li>
                        <li class="has-dropdown accordion-box">
                           <div class="drop-submenu-r"> <span class="bx bx-grid-alt nav-icon" data-bs-toggle="tooltip" data-bs-placement="right" title="Dashboard">
                                 <!--   <div class="hover-nav-bar-icon">rdfgf</div> -->
                              </span>
                              <span class="nav-text">Testimonials</span>
                              <a href=""></a>
                           </div>
                           <div class="accordion-content">
                              <ul>
                                 <li> <a href="{{route('admin.testimonial.create')}}">Add</a></li>
                                 <li> <a href="{{route('admin.testimonial.index')}}">View</a></li>
                              </ul>
                           </div>
                        </li>
                        <li class=" accordion-box">
                           <div class="drop-submenu-r"> <span class="bx bx-grid-alt nav-icon" data-bs-toggle="tooltip" data-bs-placement="right" title="Dashboard">
                                 <!--   <div class="hover-nav-bar-icon">rdfgf</div> -->
                              </span>
                              <span class="nav-text"><a href="{{route('admin.contect.index')}}">contact enquiry</a></span>
                           </div>
                        </li>
                        <li class="has-dropdown accordion-box">
                           <div class="drop-submenu-r"> <span class="bx bx-grid-alt nav-icon" data-bs-toggle="tooltip" data-bs-placement="right" title="Dashboard">
                                 <!--   <div class="hover-nav-bar-icon">rdfgf</div> -->
                              </span>
                              <span class="nav-text">Site Setting</span>
                              <a href=""></a>
                           </div>
                           <div class="accordion-content">
                              <ul>
                                 <li> <a href="{{route('admin.general.create')}}">General</a></li>
                                 <li> <a href="{{route('admin.contactde.create')}}">Contect Details</a></li>
                              </ul>
                           </div>
                        </li>
                     </ul>
                  </div>
               </nav>
            </div>
            <!-- sidbar -->
         </div>
         <div class="main-content box">
            @yield('contant')
         </div>
      </div>
   </div>
   <script src="{{url('admin/assets/js/bootstrap.bundle.min.js')}}" defer></script>
   <script src="{{url('admin/assets/js/jquery.min.js')}}" defer></script>
   <script src="{{url('admin/assets/js/togglebtn.js')}}" defer></script>
   <script src="{{url('admin/assets/js/accordion.js')}}" defer></script>
   <script src="{{url('admin/assets/js/validation.js')}}" defer></script>
   <script src="{{url('admin/assets/js/script.js')}}" defer></script>
</body>

</html>