<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Contactde;
use Illuminate\Http\Request;

class ContactdeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('backend.pages.contactde');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contactde = new Contactde();
        $contactde->number = $request->number;
        $contactde->email = $request->email;
        $contactde->address = $request->address;
        $contactde->facebooklink = $request->facebooklink;
        $contactde->instagramlink = $request->instagramlink;
        $contactde->twitterlink = $request->twitterlink;
        $contactde->save();
        
        return back()->with('success', 'Succesfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contactde  $contactde
     * @return \Illuminate\Http\Response
     */
    public function show(Contactde $contactde)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contactde  $contactde
     * @return \Illuminate\Http\Response
     */
    public function edit(Contactde $contactde)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contactde  $contactde
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contactde $contactde)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contactde  $contactde
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contactde $contactde)
    {
        //
    }
}
