<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::paginate(2);
        return view('backend.pages.page.index',['pages' => $pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->flush();
        return view('backend.pages.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $page = new Page();
        $page ->title = $request->title;
        $page ->slug = $request->slug;
        $page ->excerpt = $request->excerpt;
        $page ->description = $request->description; 
        $page ->save();

        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('/images/pages', 'public');
            $page->image = $path;
            $page->save();
        }
        
        return redirect(route('admin.page.index'));

        // dd('request');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request ,Page $page)
    {
        $request->replace($page->toArray());
        $request->flash();

        $data= compact('page');
        return view('backend.pages.page.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $page ->title = $request->title;
        $page ->slug = $request->slug;
        $page ->excerpt = $request->excerpt;
        $page ->description = $request->description; 


        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('/images/pages', 'public');
            $page->image = $path;
        }
            $page ->save();

        
        return redirect(route('admin.page.index'));
    }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  \App\Models\Page  $page
    //  * @return \Illuminate\Http\Response
    //  */
    public function destroy(Page $page)
    {   
        $page->delete();

        return redirect(route('admin.page.index'));
    }
}
