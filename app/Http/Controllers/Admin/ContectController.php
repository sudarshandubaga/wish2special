<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Contect;
use Illuminate\Http\Request;

class ContectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $contects = Contect::paginate(10);
        return view('backend.pages.contect', ['contects' => $contects,]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.pages.contect');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contect = new Contect();
        $contect->name = $request->name;
        $contect->email = $request->email;
        $contect->number = $request->number;
        $contect->message = $request->message;
        $contect->save();

        return back()->with('success', 'Succesfully Added');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contect  $contect
     * @return \Illuminate\Http\Response
     */
    public function show(Contect $contect)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contect  $contect
     * @return \Illuminate\Http\Response
     */
    public function edit(Contect $contect)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contect  $contect
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contect $contect)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contect  $contect
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contect $contect)
    {
        //
    }
}
